tactic = {}

-- Global
setTactic = 1

function tactic.update()
	TextLogAddEntry("Chat", 0, L"Settings tactic set to: " .. setTactic)
	TacticsEditor.OnSetMenuSelectionChanged(setTactic);
end

function tactic.init()
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "tactic.update")
	RegisterEventHandler(SystemData.Events.LOADING_END, "tactic.update")
end